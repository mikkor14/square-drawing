﻿using System;

namespace SquareDraw
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please enter the height of the rectangle");
            int height = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Please enter the length of the rectangle");
            int length = Int32.Parse(Console.ReadLine());

            //singleRectangle(height, length);
            multiRectangle(height, length);
        }

        public static void singleRectangle(int height, int length)
        {

            int[,] square = new int[height, length];

            for (int i = 0; i < height; i++)
            {
                Console.WriteLine("");

                for (int j = 0; j < length; j++)
                {
                    //A check to find out if it is the first or last line in the square to print a filled line
                    if (i == 0 || i == height - 1)
                    {
                        Console.Write("#");

                    }
                    else if (j == 0 || j == length - 1) //Check to determine whitespace in the square
                    {
                        Console.Write("#"); //If its the first or the last place than the fill is printed
                    }
                    else
                    {
                        Console.Write(" "); //If its the middle of the square empty space is printed
                    }
                }
            }
        }

        public static void multiRectangle(int height, int length)
        {

            int[,] square = new int[height, length];

            for (int i = 0; i < height; i++)
            {
                Console.WriteLine("");

                for (int j = 0; j < length; j++)
                {
                    if (i == 0 || i == height - 1 || j == 0 || j == length - 1 ||
                        (i == 2 && j != 1 && j != length - 2) || (i == height - 3 && j != 1 && j != length - 2) ||
                        (j == 2 && i != 1 && i != height - 2) || (j == length - 3 && i != 1 && i != height - 2)
                        )
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }


                }
            }

        }

    }
}
